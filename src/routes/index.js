import React from 'react'
import {
  BrowserRouter as Router,
  Route
} from 'react-router-dom'
import App from '../components/App'
import ArticlesListCont from '../containers/ArticlesListCont'
import ArticleComponentCont from '../containers/ArticleComponentCont'


const SetRoutes = () => (
  <Router> 
    <div>
        <Route exact path='/' component={ App } />
        <Route exact path='/articles'
               component={ ArticlesListCont } />
        <Route path='/articles/:id' component={ ArticleComponentCont }/>
    </div>
  </Router>
)

export default SetRoutes
