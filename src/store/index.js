import {createStore, applyMiddleware} from 'redux'
import createSagaMiddleware from 'redux-saga'
import rootSaga from '../saga'
import reducer from '../reducers'

export default function configureStore(data) {
    const sagaMiddleware = createSagaMiddleware();
    const middleware = [sagaMiddleware];
    
    const store = createStore(
        reducer,
        data,
        applyMiddleware(...middleware)
    );

    sagaMiddleware.run(rootSaga);

    return store
}

