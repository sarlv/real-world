import {
    FETCH_ARTICLES_REQUEST, 
    FETCH_ARTICLES_SUCCESS, 
    FETCH_ARTICLES_ERROR,
    
    FETCH_ARTICLE_REQUEST, 
    FETCH_ARTICLE_SUCCESS, 
    FETCH_ARTICLE_ERROR,
    
    FETCH_COMMENTS_REQUEST, 
    FETCH_COMMENTS_SUCCESS, 
    FETCH_COMMENTS_ERROR } from '../const' 
/**
 * Fetch Articles
 */
export const fetchArticlesRequest = () => {
  return {
    type: FETCH_ARTICLES_REQUEST 
  }
}

export const fetchArticlesSuccess = (payload) => {
  return {
    type: FETCH_ARTICLES_SUCCESS, 
    payload
  }
}

export const fetchArticlesError = (payload) => {
  return {
    type: FETCH_ARTICLES_ERROR, 
    error: payload.error
  }
}

/**
 * Fetch Article
 */
export const fetchArticleRequest = (id) => {
  return {
    type: FETCH_ARTICLE_REQUEST,
    id
  }
}

export const fetchArticleSuccess = (payload) => {
  return {
    type: FETCH_ARTICLE_SUCCESS, 
    payload
  }
}

export const fetchArticleError = (payload) => {
  return {
    type: FETCH_ARTICLE_ERROR, 
    error: payload.error
  }
}

/**
 * Fetch Article Comments
 */
export const fetchCommentsRequest = (id) => {
  return {
    type: FETCH_COMMENTS_REQUEST, 
    id 
  }
}

export const fetchCommentsSuccess = (payload) => {
  return {
    type: FETCH_COMMENTS_SUCCESS, 
    payload 
  }
}

export const fetchCommentsError = (payload) => {
  return {
    type: FETCH_COMMENTS_ERROR, 
    error: payload.error
  }
}



