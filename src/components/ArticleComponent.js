import React, { Component } from 'react'
import PropTypes from 'prop-types'
import ArticleWithCommentsComponent from './ArticleWithCommentsComponent'

class ArticleComponent extends Component {
    componentWillMount() { 
        let { match } = this.props
        this.setState({ id: match.params.id });
        this.props.articleRequest(match.params.id);
        this.props.articleSuccess();
    }

    showComments = () => {
        this.props.commentsRequest(this.state.id);
        this.props.commentsSuccess();

    }

    render() {
      let { article, comments } = this.props
      return (
          <div>
          {
            article &&
              <div>
                  <h1>{ article.title }</h1>
                  <h2>{ article.author}</h2>
                  {
                    
                  }  
                  <p onClick={ this.showComments }>show comments</p>
              </div>
          }
          {
            comments &&
                  <ArticleWithCommentsComponent comment={ comments.body } />
          }
          </div>
      )
    }
}

ArticleComponent.propTypes = {
    article: PropTypes.object
}

export default ArticleComponent
