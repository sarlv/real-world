import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Link } from 'react-router-dom'

class ArticleList extends Component {
    componentWillMount() { 
        this.props.fetchRequest();
        this.props.articleSuccess();
    }

	render(){
      let { articles, match } = this.props;
	  return (
            <div>
            <h1>Articles</h1>
            <ul>
				{
                    articles &&
                    articles.map(post => {
                        return(
                            <div key={ post.id }>
                            <li>
                                <Link to={`${match.path}/${post.id}`}>
                                <b>title:</b> {post.title}
                                <b>author:</b> {post.author}
                                </Link>
                            </li>
                            </div>
                        )
                    })
                }
            </ul>
            </div>
      )
    }
}

ArticleList.propTypes = {
    articles: PropTypes.array, 
    match: PropTypes.object.isRequired 
}

export default ArticleList
