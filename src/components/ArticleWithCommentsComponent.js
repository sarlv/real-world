import React from 'react'
import PropTypes from 'prop-types'

const ArticleWithCommentsComponent = ({ comment }) => ( 
      <div>
        <p>{ comment }</p>
      </div>
)

ArticleWithCommentsComponent.propTypes = { 
    comment: PropTypes.string
}

export default ArticleWithCommentsComponent
