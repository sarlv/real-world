import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Link } from 'react-router-dom'

class ArticleList extends Component {
	render(){
	  return (
          <div>
            <h1>My supper title ;)</h1>
            <p><Link to='articles'>Show articles</Link></p>
          </div>
      )
    }
}

ArticleList.propTypes = {
    articles: PropTypes.array 
}

export default ArticleList
