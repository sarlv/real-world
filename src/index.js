import React from 'react'
import { render } from 'react-dom'
import configureStore from './store'
import Root from './containers/Root'

render(
    <Root 
        store={ configureStore() } 
    />,
    document.getElementById('root')
);

