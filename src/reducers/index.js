import {
    FETCH_ARTICLES_REQUEST, 
    FETCH_ARTICLES_SUCCESS, 
    FETCH_ARTICLES_ERROR,
    
    FETCH_ARTICLE_REQUEST, 
    FETCH_ARTICLE_SUCCESS, 
    FETCH_ARTICLE_ERROR,
    
    FETCH_COMMENTS_REQUEST, 
    FETCH_COMMENTS_SUCCESS, 
    FETCH_COMMENTS_ERROR,
} from '../const'

const reducer = (state = {}, action) => {
  switch (action.type) {
    case FETCH_ARTICLES_REQUEST:
      return state;
    case FETCH_ARTICLES_SUCCESS: 
      return {...state, articles: action.articles };
    case FETCH_ARTICLES_ERROR:
      return state;
    case FETCH_ARTICLE_REQUEST:
      return state;
    case FETCH_ARTICLE_SUCCESS: 
      return {...state, article: action.article };
    case FETCH_ARTICLE_ERROR:
      return state;
    case FETCH_COMMENTS_REQUEST:
      return state;
    case FETCH_COMMENTS_SUCCESS: 
      return {...state, comments: action.comments };
    case FETCH_COMMENTS_ERROR:
      return state;
    default:
      return state;
  }
}

export default reducer
