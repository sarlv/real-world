import {connect} from 'react-redux'
import ArticlesList from '../components/ArticlesList' 
import { 
    fetchArticlesRequest,
    fetchArticlesSuccess
} from '../actions'

const mapStateToProps = state => {
  return {
  	articles: state.articles
  }
}

const mapDispatchToProps = dispatch => {
  return {
    fetchRequest: () => {
      dispatch(fetchArticlesRequest())
    },
    articleSuccess: (data) => {
      dispatch(fetchArticlesSuccess(data))
    }
  }
}

let Root = connect(mapStateToProps, mapDispatchToProps)(ArticlesList);

export default Root 
