import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Provider } from 'react-redux'
import SetRoutes from '../routes'

export default class Root extends Component {
  render() {
    const { store } = this.props

    return (
        <Provider store={ this.props.store }>
            <SetRoutes />
        </Provider>
     )
  }
}

Root.propTypes = {
  store: PropTypes.object.isRequired,
}

