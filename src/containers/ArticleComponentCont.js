import {connect} from 'react-redux'
import ArticleComponent from '../components/ArticleComponent' 
import { 
    fetchArticleRequest,
    fetchArticleSuccess,
    fetchCommentsRequest,
    fetchCommentsSuccess
} from '../actions'

const mapStateToProps = state => {
  return {
  	article: state.article,
  	comments: state.comments
  }
}

const mapDispatchToProps = dispatch => {
  return {
    articleRequest: (id) => {
      dispatch(fetchArticleRequest(id))
    },
    articleSuccess: (data) => {
      dispatch(fetchArticleSuccess(data))
    },
    commentsRequest: (id) => {
      dispatch(fetchCommentsRequest(id))
    },
    commentsSuccess: () => {
      dispatch(fetchCommentsSuccess())
    }
  }
}

let ArticleComponentCont = connect(mapStateToProps, mapDispatchToProps)(ArticleComponent);

export default ArticleComponentCont
