import { takeEvery, call, put } from 'redux-saga/effects'
import axios from 'axios'
import {
    ROOT_URL,
    FETCH_ARTICLES_REQUEST, 
    FETCH_ARTICLES_SUCCESS, 
    FETCH_ARTICLES_ERROR
} from '../const'


export function* fetchArticlesAsync() {
    try {
        const articles = yield call(
            axios.get, 
            `${ ROOT_URL }/articles`
        )
        yield put({ type: FETCH_ARTICLES_SUCCESS, articles: articles.data })
    } catch(e) {
        console.log('Some Error was happen', e);
        yield put({ type: FETCH_ARTICLES_ERROR, error: e })
    }
}

export function* watchArticlesFetch() {
    yield takeEvery(
        FETCH_ARTICLES_REQUEST,
        fetchArticlesAsync
    );
}











