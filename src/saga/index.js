import { watchArticlesFetch } from './articles'
import { watchArticleFetch } from './article'
import { watchCommentsFetch } from './comments'

export default function* rootSaga() {
    yield [
        watchArticlesFetch(),
        watchArticleFetch(),
        watchCommentsFetch()
    ]
}











