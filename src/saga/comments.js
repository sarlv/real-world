import { takeEvery, call, put } from 'redux-saga/effects'
import axios from 'axios'
import {
    ROOT_URL,
    FETCH_COMMENTS_REQUEST, 
    FETCH_COMMENTS_SUCCESS, 
    FETCH_COMMENTS_ERROR
} from '../const'


export function* fetchCommentsAsync(data) {
    let { id } = data;
    try {
        const comments = yield call(
            axios.get, 
            `${ ROOT_URL }/comments/${ id }`
        )
        yield put({ type: FETCH_COMMENTS_SUCCESS, comments: comments.data })
    } catch(e) {
        console.log('Some Error was happen', e);
        yield put({ type: FETCH_COMMENTS_ERROR, error: e })
    }
}

export function* watchCommentsFetch() {
    yield takeEvery(
        FETCH_COMMENTS_REQUEST,
        fetchCommentsAsync
    );
}











