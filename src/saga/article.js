import { takeEvery, call, put } from 'redux-saga/effects'
import axios from 'axios'
import {
    ROOT_URL,
    FETCH_ARTICLE_REQUEST, 
    FETCH_ARTICLE_SUCCESS, 
    FETCH_ARTICLE_ERROR
} from '../const'

export function* fetchArticleAsync(data) {
    let { id } = data;
    try {
        const article = yield call(
            axios.get, 
            `${ ROOT_URL }/articles/${ id }`
        )
        yield put({ type: FETCH_ARTICLE_SUCCESS, article: article.data })
    } catch(e) {
        console.log('Some Error was happen', e);
        yield put({ type: FETCH_ARTICLE_ERROR, error: e })
    }
}

export function* watchArticleFetch() {
    yield takeEvery(
        FETCH_ARTICLE_REQUEST,
        fetchArticleAsync
    );
}











